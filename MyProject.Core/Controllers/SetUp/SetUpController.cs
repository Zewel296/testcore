﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MyProject.Core.Controllers.SetUp
{
    public class SetUpController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Bank()
        {
            return View();
        }

        public IActionResult LogIn()
        {
            return View();
        }
    }
}