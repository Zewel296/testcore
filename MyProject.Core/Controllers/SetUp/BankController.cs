﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyProject.Core.Library.Models;
using MyProject.Core.Library.UnitOfWork;
using Microsoft.AspNetCore.DataProtection;
namespace MyProject.Core.Controllers.SetUp
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/Bank")]
    
    public class BankController : BaseController
    {
        IDataProtector _iDP;
        public BankController(IUnitOfWork uow, IDataProtectionProvider dataProtectionProvider)
        {
            Uow = uow;
            _iDP = dataProtectionProvider.CreateProtector("BanKClass");
        }
        /// <summary>
        /// Get:: /api/bank/Getall
        /// </summary>
        /// <returns></returns>
        /// 
        
        [HttpGet("GetAll")]
        public async Task<IEnumerable<Bank>> GetAll()
        {
            //var remoteIpAddress = Request.UserHostAddress;
            var mac = GetMACAddress();
            var ip = GetIPAddress();
            string name = "ABC";
            string proString = _iDP.Protect(name); //Microsoft Encryption Method for data security;
            string DString = _iDP.Unprotect(proString);
            var q = await Uow._Bank.GetAll();
            return q;
        }
        public string GetMACAddress()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
            IEnumerable<ManagementObject> objects = searcher.Get().Cast<ManagementObject>();
            string mac = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();
            //lblMacId.Text = mac;

            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            IFormatProvider theCultureInfo = new System.Globalization.CultureInfo("en-GB", true);
            string dt = Convert.ToDateTime(DateTime.Now, theCultureInfo).ToString("MM-dd-yyyy hh:mm tt");
            return sMacAddress;

        }
        public string GetIPAddress()
        {
            string ip = String.Empty;
            IPHostEntry Host = default(IPHostEntry);
            string Hostname = null;
            Hostname = System.Environment.MachineName;
            Host = Dns.GetHostEntry(Hostname);
            foreach (IPAddress IP in Host.AddressList)
            {
                if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    ip = Convert.ToString(IP);
                }
            }
            return ip;
        }
       
    }
}