﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyProject.Core.Library.Models;
using MyProject.Core.Library.UnitOfWork;

namespace MyProject.Core.Controllers.Security
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/Security")]
    public class SecurityController : BaseController
    {
        IDataProtector _iDP;
        public SecurityController(IUnitOfWork uow, IDataProtectionProvider dataProtectionProvider)
        {
            Uow = uow;
            _iDP = dataProtectionProvider.CreateProtector("SecurityClass");
        }
        [HttpGet("GetAll")]
        public async Task<IEnumerable<Bank>> GetAll()
        {
            //var remoteIpAddress = Request.UserHostAddress;
            string name = "ABC";
            string proString = _iDP.Protect(name); //Microsoft Encryption Method for data security;
            string DString = _iDP.Unprotect(proString);
            var q = await Uow._Bank.GetAll();
            return q;
            
        }
    }
}