﻿using System;
using System.Collections.Generic;

namespace MyProject.Core.Library.Models
{
    public partial class Bank
    {
        public int BankId { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string PhoneNo { get; set; }
    }
}
