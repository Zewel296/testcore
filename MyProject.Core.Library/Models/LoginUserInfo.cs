﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MyProject.Core.Library.Models
{
    public class LoginUserInfo
    {
        [Key]
        public int LoginUserId { get; set; }

        public string LoginUserName { get; set; }

        public string LoginUserType { get; set; }

        public string LoginUserEmpId { get; set; }

        public string LoginPassword { get; set; }

        public bool IsActive { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string Designation { get; set; }

        public string Department { get; set; }

        public string EmpName { get; set; }
    }
}
