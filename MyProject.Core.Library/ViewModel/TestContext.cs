﻿using Microsoft.EntityFrameworkCore;
using MyProject.Core.Library.Models;

namespace MyProject.Core.Library.ViewModel 
{
    public partial class TestContext : DbContext
    {
        public TestContext()
        {
        }
       

        public TestContext(DbContextOptions<TestContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Bank> Bank { get; set; }

        public virtual DbSet<LoginUserInfo> LoginUserInfo { get; set; }

        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //            if (!optionsBuilder.IsConfigured)
        //            {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //                optionsBuilder.UseSqlServer("Data Source=(local);Initial Catalog=Test;Integrated Security=True");
        //            }
        //        }


    }
}
