﻿using MyProject.Core.Library.Models;
using MyProject.Core.Library.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyProject.Core.Library.UnitOfWork
{
    public interface IUnitOfWork
    {

        IRepository<Bank> _Bank { get; }
        IRepository<LoginUserInfo> _LoginUserInfo  { get; }
    }
}
