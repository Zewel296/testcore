﻿using MyProject.Core.Library.Models;
using MyProject.Core.Library.Repository;
using System;
using MyProject.Core.Library.ViewModel;

namespace MyProject.Core.Library.UnitOfWork
{
    public class UnitOfWork:IUnitOfWork,IDisposable
    {
        private TestContext context { get; set; }
        public UnitOfWork(TestContext context)
        {
            this.context = context;
        }
        private IRepository<Bank> _bank;
        public IRepository<Bank> _Bank
        {
            get
            {
                if (this._bank == null)
                {
                    this._bank = new GenericRepository<Bank>(context);
                    return _bank;
                }
                else
                {
                    return _bank;
                }
            }
        }
        private IRepository<LoginUserInfo> _loginUserInfo;
        public IRepository<LoginUserInfo> _LoginUserInfo
        {
            get
            {
                if (this._loginUserInfo == null)
                {
                    this._loginUserInfo = new GenericRepository<LoginUserInfo>(context);
                    return _loginUserInfo;
                }
                else
                {
                    return _loginUserInfo;
                }
            }
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
